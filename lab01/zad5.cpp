#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int maksimum(int brojevi[], int n) {
    int maks = brojevi[0];
    for (int i = 0; i < n; i++) {
        if (maks < brojevi[i]) {
            maks = brojevi[i];
        }
    }
    return maks;
}

double prosjek(int brojevi[], int n) {
    int prosjek = 0;
    for (int i = 0; i < n; i++) {
        prosjek += brojevi[i];
    }
    return prosjek / static_cast<double>(n);
}

int main() {
    int n;
    int brojevi[100];
    cout << "Unesite N: ";
    cin >> n;
    for (int i = 0; i < n; i++) {
        cout << "Unesite " << i + 1 << ". vrijednost: ";
        cin >> brojevi[i];
    }
    cout << "Maksimalna vrijednost: " << maksimum(brojevi, n) << endl;
    cout << "Prosjecna vrijednost: " << prosjek(brojevi, n) << endl;
}