#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int main() {
    int n;
    int brojevi[100];
    cout << "Unesite N: ";
    cin >> n;

    for (int i = 0; i < n; i++) {
        cout << "Unesite " << i + 1 << ". vrijednost: ";
        cin >> brojevi[i];
    }

    int maks = brojevi[0];
    int prosjek = 0;
    for (int i = 0; i < n; i++) {
        if (maks < brojevi[i]) {
            maks = brojevi[i];
        }
        prosjek += brojevi[i];
    }
    
    cout << "Maksimalna vrijednost: " << maks << endl;
    cout << "Prosjecna vrijednost: " << prosjek / static_cast<double>(n) <<
    endl;
}