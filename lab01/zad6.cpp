#include <iostream>

using std::cout;
using std::cin;
using std::endl;

void maksimum(int brojevi[], int n, int* maks) {
    *maks = brojevi[0];
    for (int i = 0; i < n; i++) {
        if (*maks < brojevi[i]) {
            *maks = brojevi[i];
        }
    }
}

double prosjek(int brojevi[], int n) {
    int prosjek = 0;
    for (int i = 0; i < n; i++) {
        prosjek += brojevi[i];
    }
    return prosjek / static_cast<double>(n);
}

int main() {
    int n;
    int brojevi[100];
    int maks;
    cout << "Unesite N: ";
    cin >> n;
    for (int i = 0; i < n; i++) {
        cout << "Unesite " << i + 1 << ". vrijednost: ";
        cin >> brojevi[i];
    }
    maksimum(brojevi, n, &maks);
    cout << "Maksimalna vrijednost: " << maks << endl;
    cout << "Prosjecna vrijednost: " << prosjek(brojevi, n) << endl;
}