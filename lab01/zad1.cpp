#include <iostream>
#include <string>

using std::cout;
using std::cin;
using std::endl;
using std::string;

int main() {
    int cijeli;
    double decimalni;
    string ime;

    cout << "Unesite cjelobrojnu vrijednost: ";
    cin >> cijeli;
    cout << "Unesite decimalnu vrijednost: ";
    cin >> decimalni;
    cout << "Unesite Vase ime: ";
    cin >> ime;
    cout << "Pozdrav, " << ime << endl;

    if (decimalni > cijeli) {
        cout << "Decimalna vrijednost je veca od cjelobrojne vrijednosti." << endl;
    } else {
        cout << "Decimalna vrijednost je manja od cjelobrojne vrijednosti." << endl;
    }
}