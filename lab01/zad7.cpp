#include <iostream>

using std::cout;
using std::endl;

int main() {
    int polje[5] = {4, -2, 7, -9, 6};
    int broj;
    
    int* p = polje;
    cout << *p << endl;

    *p++;
    (*p)++;
    cout << *p << endl;

    broj = *p;
    broj = broj + 10;
    cout << *p << endl;

    *p = broj + 3;
    cout << *(p-1) << endl;
    cout << *p - 1 << endl;
    cout << *++p << endl;
    cout << *p << endl;
}