#include <iostream>
#include <string>
#include <cmath>

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::pow;

int main() {
    int cijeli;
    double decimalni;
    string ime;

    cout << "Unesite cjelobrojnu vrijednost: ";
    cin >> cijeli;
    cout << "Unesite decimalnu vrijednost: ";
    cin >> decimalni;
    cout << "Unesite Vase ime: ";
    cin >> ime;
    cout << "Pozdrav, " << ime << endl;

    if (decimalni > cijeli) {
        cout << "Decimalna vrijednost je veca od cjelobrojne vrijednosti." << endl;
    } else {
        cout << "Decimalna vrijednost je manja od cjelobrojne vrijednosti." << endl;
    }
    cout << "Potencija: (" << decimalni << "^" << cijeli << ") = " << pow(decimalni, cijeli) << endl;

    for (int i = cijeli; i <= round(decimalni); i++) {
        if (i % 2 == 0) {
            cout << i << " ";
        }
    }

    cout << endl;
    int i = cijeli;
    while (i <= round(decimalni)) {
        if (i % 2) {
            cout << i << " ";
        }
        i++;
    }
    cout << endl;
}