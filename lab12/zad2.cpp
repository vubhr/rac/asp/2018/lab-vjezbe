#include <iostream>
#include <vector>
#include <algorithm>
#include <chrono>

using std::cin;
using std::cout;
using std::endl;
using std::random_shuffle;
using std::vector;

void mergeSort(vector<int> &left, vector<int> &right, vector<int> &nums) {
    int nL = left.size();
    int nR = right.size();
    int i = 0, j = 0, k = 0;

    while (j < nL && k < nR) {
        if (left[j] < right[k]) {
            nums[i] = left[j];
            j++;
        } else {
            nums[i] = right[k];
            k++;
        }
        i++;
    }
    while (j < nL) {
        nums[i] = left[j];
        j++;
        i++;
    }
    while (k < nR) {
        nums[i] = right[k];
        k++;
        i++;
    }
}

void sort(vector<int> &nums) {
    bool sorted = true;
    for (int i = 0; i < nums.size() - 1; i++) {
        if (nums[i] > nums[i+1]) {
            sorted = false;
            break;
        }
    }
    if (sorted) {
        return;
    }
    if (nums.size() < 2) {
        return;
    }

    int mid = nums.size() / 2;
    vector<int> left;
    vector<int> right;

    for (int i = 0; i < mid; i++)
        left.push_back(nums[i]);
    for (int i = 0; i < (nums.size()) - mid; i++)
        right.push_back(nums[mid + i]);
    sort(left);
    sort(right);
    mergeSort(left, right, nums);
}

int main() {
    vector<int> v1;
    for (int i = 1; i <= 100000; i++)
    {
        v1.push_back(i);
    }
    random_shuffle(v1.begin(), v1.end());

    vector<int> v2{v1};

    std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();
    sort(v1);
    std::chrono::high_resolution_clock::time_point stop = std::chrono::high_resolution_clock::now();
    cout << "Trajanje merge sorta: " << std::chrono::duration_cast<std::chrono::milliseconds>(stop - start).count() << "ms" << endl;

    start = std::chrono::high_resolution_clock::now();
    std::sort(v2.begin(), v2.end());
    stop = std::chrono::high_resolution_clock::now();
    cout << "Trajanje ugradjenog sorta: " << std::chrono::duration_cast<std::chrono::milliseconds>(stop - start).count() << "ms" << endl;
}