/*
Program ne provjerava da li se u imeniku već nalazi osoba koja se unosi!
*/
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using std::cout;
using std::endl;
using std::ifstream;
using std::string;
using std::to_string;
using std::vector;

struct Osoba {
    string prezime;
    string ime;
    string broj;
};

int main() {
    vector<Osoba> imenik = {};

    ifstream datoteka("imenik.csv");

    if (datoteka.is_open()) {
        string linija;
        string prezime;
        string ime;
        string broj;
        
        getline(datoteka, linija);
        while (getline(datoteka, linija)) {
            // substr vraća sadržaj iz varijable linija od početka do pozicije prvog zareza, tj. prezime
            prezime = linija.substr(0, linija.find(","));
            // brišemo sadržaj iz linije od početka do prvog zareza, uključujući i zarez (Badric,Nina,098233426 => Nina,098233426)
            linija.erase(0, linija.find(",") + 1);
            ime = linija.substr(0, linija.find(","));
            // (Nina,098233426 => 098233426)
            linija.erase(0, linija.find(",") + 1);
            broj = linija;

            // unos osobe na kraj imenika
            imenik.push_back({ prezime, ime, broj });
        }
        datoteka.close();
    }

    cout << "Osoba u imeniku: " << imenik.size() << endl;
}
