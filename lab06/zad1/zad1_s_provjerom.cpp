#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using std::cout;
using std::endl;
using std::ifstream;
using std::string;
using std::to_string;
using std::vector;

struct Osoba {
    string prezime;
    string ime;
    string broj;
};

// osobaUImeniku vraća true ukoliko se osoba nalazi u imeniku, inače false
bool osobaUImeniku(const vector<Osoba>& imenik, string prezime, string ime) {
    for (auto osoba : imenik) {
        if (osoba.prezime == prezime && osoba.ime == ime) {
            return true;
        }
    }
    return false;
}

int main() {
    vector<Osoba> imenik = {};

    ifstream datoteka("imenik.csv");

    if (datoteka.is_open()) {
        string linija;
        string prezime;
        string ime;
        string broj;
        
        getline(datoteka, linija);
        while (getline(datoteka, linija)) {
            prezime = linija.substr(0, linija.find(","));
            linija.erase(0, linija.find(",") + 1);
            ime = linija.substr(0, linija.find(","));
            linija.erase(0, linija.find(",") + 1);
            broj = linija;

            if (!osobaUImeniku(imenik, prezime, ime)) {
                imenik.push_back({ prezime, ime, broj });
            }
        }
        datoteka.close();
    }

    cout << "Osoba u imeniku: " << imenik.size() << endl;
}
