#include <fstream>
#include "Imenik.h"

using std::string;
using std::ifstream;

Imenik::Imenik(string imeDatoteke) {
    ifstream datoteka(imeDatoteke);

    if (datoteka.is_open()) {
        string linija;
        string prezime;
        string ime;
        string broj;
        
        getline(datoteka, linija);
        while (getline(datoteka, linija)) {
            prezime = linija.substr(0, linija.find(","));
            linija.erase(0, linija.find(",") + 1);
            ime = linija.substr(0, linija.find(","));
            linija.erase(0, linija.find(",") + 1);
            broj = linija;

            if (!osobaUImeniku(prezime, ime)) {
                imenik.push_back({ prezime, ime, broj });
            }
        }
        datoteka.close();
    }    
}

bool Imenik::osobaUImeniku(string prezime, string ime) {
    for (auto osoba : imenik) {
        if (osoba.prezime == prezime && osoba.ime == ime) {
            return true;
        }
    }
    return false;
}

string Imenik::dohvatiBroj(string prezime, string ime) {
    for (auto osoba : imenik) {
        if (osoba.prezime == prezime && osoba.ime == ime) {
            return osoba.broj;
        }
    }
    return "NE POSTOJI";
}