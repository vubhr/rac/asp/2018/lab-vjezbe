#include <iostream>
#include "Imenik.h"

using std::cout;
using std::endl;

int main() {
    Imenik imenik("imenik.csv");

    cout << imenik.dohvatiBroj("Badric", "Nina") << endl;
    cout << imenik.dohvatiBroj("Houdek", "Zak") << endl;
}