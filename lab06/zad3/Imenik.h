#pragma once

#include <string>
#include <vector>

struct Osoba {
    std::string prezime;
    std::string ime;
    std::string broj;
};

class Imenik {
public:
    Imenik(std::string);

    bool osobaUImeniku(std::string, std::string);
    std::string dohvatiBroj(std::string, std::string);
private:
    std::vector<Osoba> imenik;
};