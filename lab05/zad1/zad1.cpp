#include <iostream>
#include <array>
 
using std::cout;
using std::cin;
using std::endl;
using std::array;
 
int main() {
    int n = 0;
    array<int, 100> polje = {};
 
    while (n < 1 || n > 100) {
        cout << "Unesite broj N: ";
        cin >> n;
    }
 
    int vrijednost;
    for (int i = 0; i < n; i++) {
        vrijednost = -101;
        while (vrijednost < -100 || vrijednost > 100) {
            cout << "Unesite vrijednost u intervalu [-100,100]: ";
            cin >> vrijednost;
        }
        polje[i] = vrijednost;
    }
 
    // petlja prikazuje sve elemente, ne samo N elemenata!
    for (int i = 0; i < polje.size(); i++) {
        cout << polje.at(i) << endl;
    }
}
