#include <iostream>
#include "Polje.h"
 
using std::cout;
using std::endl;
 
int main() {
    Polje polje(50);
    polje.fill(1);
    cout << polje.at(0) << endl;
 
    polje.fill(4);
    cout << polje.front() << endl;
 
    polje.fill(5);
    cout << polje.back() << endl;
 
    cout << polje.size() << endl;
}
