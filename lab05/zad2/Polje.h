#pragma once
 
class Polje {
public:
    Polje();
    Polje(int);
    int at(int);
    int size();
    void fill(int);
    int front();
    int back();
 
private:
    int polje[1000];
    int velicina;
};
