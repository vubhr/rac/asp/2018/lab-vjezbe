#include "Polje.h"
 
Polje::Polje() {
    this->velicina = 1000;
}
 
Polje::Polje(int velicina) {
    this->velicina = velicina;
}
 
int Polje::at(int index) {
    return polje[index];
}
 
int Polje::size() {
    return velicina;
}
 
void Polje::fill(int vrijednost) {
    for (int i = 0; i < velicina; i++) {
        polje[i] = vrijednost;
    }
}
 
int Polje::front() {
    return polje[0];
}
 
int Polje::back() {
    return polje[velicina - 1];
}
