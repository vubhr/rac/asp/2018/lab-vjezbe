#include <iostream>
#include <vector>

using std::cout;
using std::cin;
using std::endl;
using std::vector;

int main() {
    vector<double> v;

    double unos;
    while (true) {
        cout << "Unesite broj: ";
        cin >> unos;
        if (unos == 0) {
            break;
        }
        v.push_back(unos);
    }

    double tmp;
    for (int i = 0; i < v.size(); i++) {
        for (int j = 0; j < v.size() - 1; j++) {
            if (v[j] > v[j+1]) {
                tmp = v[j];
                v[j] = v[j+1];
                v[j+1] = tmp;
            }
        }
    }

    for (double broj : v) {
        cout << broj << endl;
    }
}