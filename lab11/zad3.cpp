#include <iostream>
#include <vector>
#include <string>

using std::cout;
using std::cin;
using std::endl;
using std::vector;
using std::string;

int main() {
    vector<string> v;

    string unos;
    while (true) {
        cout << "Unesite rijec: ";
        cin >> unos;
        if (unos == "0") {
            break;
        }
        v.push_back(unos);
    }

    string tmp;
    for (int i = 0; i < v.size(); i++) {
        for (int j = 0; j < v.size() - 1; j++) {
            if (v[j] > v[j+1]) {
                tmp = v[j];
                v[j] = v[j+1];
                v[j+1] = tmp;
            }
        }
    }

    for (string rijec : v) {
        cout << rijec << endl;
    }
}