#include <iostream>
#include <vector>
#include <algorithm>

using std::cout;
using std::cin;
using std::endl;
using std::vector;
using std::random_shuffle;
using std::sort;

int main() {
    vector<int> v1;
    for (int i = 1; i <= 10000; i++) {
        v1.push_back(i);
    }
    random_shuffle(v1.begin(), v1.end());

    vector<int> v2{v1};

    cout << "Pokrecem bubble sort..." << endl;
    int tmp;
    for (int i = 0; i < v1.size(); i++) {
        for (int j = 0; j < v1.size() - 1; j++) {
            if (v1[j] > v1[j+1]) {
                tmp = v1[j];
                v1[j] = v1[j+1];
                v1[j+1] = tmp;
            }
        }
    }
    cout << "Zavrsen bubble sort" << endl;

    cout << "Pokrecem ugradjeni sort" << endl;
    sort(v2.begin(), v2.end());
    cout << "Zavrsen ugradjeni sort" << endl;
}