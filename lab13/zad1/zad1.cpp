#include <iostream>
#include <fstream>
#include <string>
#include <unordered_map>

using std::cout;
using std::endl;
using std::ifstream;
using std::string;
using std::unordered_map;
using std::stoi;

int main() {
    unordered_map<int, string> mjesta;
    int postanskiBrojZaUnos = 0;
    int postanskiBroj;
    string naselje;

    ifstream datoteka("mjesta.csv");
    string linija;
    if (datoteka.is_open()) {
        while (getline(datoteka, linija)) {
            postanskiBroj = stoi(linija.substr(0, linija.find(";")));
            linija.erase(0, linija.find(";") + 1);
            naselje = linija.substr(0, linija.length() - 1);

            if (postanskiBroj != postanskiBrojZaUnos) {
                postanskiBrojZaUnos = postanskiBroj;
                mjesta[postanskiBroj] = naselje;
            }
        }
        datoteka.close();
    }

    for (auto mjesto : mjesta) {
        cout << mjesto.first << " " << mjesto.second << endl;
    }
}