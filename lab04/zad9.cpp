#include <iostream>
#include "Tocka.h"

using std::cout;

int main() {
    Tocka t1;
    Tocka t2(2, 3);
    t1.postavi_koordinate(1, 2);
    cout << t1.koordinate();
}