class Tocka {
public:
    Tocka() {
        x = 0;
        y = 0;
    }
    Tocka(int x, int y) {
        this ->x = x;
        this ->y = y;
    }

private:
    int x;
    int y;
};

int main() {
    Tocka t1;
    Tocka t2(2, 3);
}