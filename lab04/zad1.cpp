#include <iostream>

using std::cout;

struct Tocka {
    int x;
    int y;
};

void prikaziTocku(Tocka t) {
    cout  << "(" << t.x << "," << t.y << ")";
}

int main() {
    Tocka t1 = { 2, 3 };
    prikaziTocku(t1);
}