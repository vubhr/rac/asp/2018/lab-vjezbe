#include <iostream>
#include <cmath>

using std::cout;
using std::sqrt;
using std::pow;

struct Tocka {
    int x;
    int y;
};

void prikaziTocku(Tocka t) {
    cout  << "(" << t.x << "," << t.y << ")";
}

double udaljenost(Tocka t1, Tocka  t2) {
    return  sqrt(pow(t2.x - t1.x, 2) + pow(t2.y - t1.y, 2));
}

int main() {
    Tocka t1 = { 2, 3 };
    Tocka t2 = { 4, 5 };
    prikaziTocku(t1);
    cout << udaljenost(t1, t2);
}