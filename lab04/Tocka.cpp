#include <cmath>
#include "Tocka.h"

using std::sqrt;
using std::to_string;

Tocka::Tocka() {
    x = 0;
    y = 0;
}

Tocka::Tocka(int x, int y) {
    this->x = x;
    this->y = y;
}

string Tocka::koordinate() {
    return "(" + to_string(x) + "," + to_string(y) + ")";
}

void Tocka::postavi_koordinate(int x, int y) {
    this->x = x;
    this->y = y;
}

double Tocka::udaljenost(Tocka t) {
    return sqrt(pow(t.x - x, 2) + pow(t.y - y, 2));
}

Tocka Tocka::sredina(Tocka t) {
    return Tocka((t.x + x) / 2, (t.y + y) / 2);
}