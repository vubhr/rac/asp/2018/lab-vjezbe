#pragma once

#include <string>

using std::string;

class Tocka {
public:
    Tocka();
    Tocka(int, int);
    double udaljenost(Tocka);
    Tocka sredina(Tocka);
    string koordinate();
    void postavi_koordinate(int, int);

private:
    int x;
    int y;
};