#include <iostream>
#include <string>
#include <cmath>

using std::cout;
using std::string;
using std::to_string;
using std::sqrt;
using std::pow;

class Tocka {
public:
    Tocka() {
        x = 0;
        y = 0;
    }
    Tocka(int x, int y) {
        this ->x = x;
        this ->y = y;
    }

    string koordinate() {
        return "(" + to_string(x) + "," + to_string(y) + ")";
    }
    void postaviKoordinate(int x, int y) {
        this ->x = x;
        this ->y = y;
    }
    double udaljenost(Tocka t) {
        return  sqrt(pow(t.x - x, 2) + pow(t.y - y, 2));
    }
    Tocka sredina(Tocka t) {
        return  Tocka((t.x + x) / 2, (t.y + y) / 2);
    }    

private:
    int x;
    int y;
};

int main() {
    Tocka t1;
    Tocka t2(2, 3);
    t1.postaviKoordinate(1, 2);
    cout << t1.koordinate();
}