#include <iostream>
#include <string>

using std::cout;
using std::string;
using std::to_string;

class Tocka {
public:
    Tocka() {
        x = 0;
        y = 0;
    }
    Tocka(int x, int y) {
        this ->x = x;
        this ->y = y;
    }

    string koordinate() {
        return "(" + to_string(x) + "," + to_string(y) + ")";
    }

private:
    int x;
    int y;
};

int main() {
    Tocka t1;
    Tocka t2(2, 3);
    cout << t2.koordinate();
}