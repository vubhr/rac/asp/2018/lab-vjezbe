#include <iostream>

using std::cout;

int zbrojZnam(int n) {
    if (n == 0) {
        return 0;
    }
    return (n % 10) + zbrojZnam(n / 10);
}

int main() {
    cout << zbrojZnam(329);
}