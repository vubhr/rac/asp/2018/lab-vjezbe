#include <iostream>

using std::cout;

void prikazi2(int n) {
    if (n > 0) {
        cout  << n;
        prikazi2(n - 1);
    }
}

int main() {
    prikazi2(5);
}