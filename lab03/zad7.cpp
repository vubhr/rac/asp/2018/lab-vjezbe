#include <iostream>

using std::cout;

void prikaziPolje(int polje[], int n) {
    if (n > 0) {
        prikaziPolje(polje , n-1);
        cout  << polje[n - 1] << " ";
    }
}

int main() {
    int polje[5] = { 3, 2, -7, 8, 12 };
    prikaziPolje(polje, 5);
}