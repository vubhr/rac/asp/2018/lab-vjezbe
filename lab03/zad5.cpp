#include <iostream>

using std::cout;

int brZnam(int n) {
    if (n == 0) {
        return 0;
    }
    return 1 + brZnam(n / 10);
}

int main() {
    cout << brZnam(329);
}