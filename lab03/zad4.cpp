#include <iostream>

using std::cout;

int suma2(int n) {
    if (n < 1) {
        return 0;
    }
    if (n % 2 == 0) {
        return n + suma2(n - 1);
    } else {
        return  suma2(n - 1);
    }
}

int main() {
    cout << suma2(5);
}