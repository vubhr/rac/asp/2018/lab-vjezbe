#include <iostream>
#include <set>

using std::cout;
using std::cin;
using std::endl;
using std::set;

bool unesi(set<int>& brojevi, int broj) {
    auto rezultatUnosa = brojevi.insert(broj);
    return rezultatUnosa.second;
}

int main() {
    set<int> brojevi;

    for (int i = 1; i <= 100; i++) {
        brojevi.insert(i);
    }

    int brojZaObrisat;
    cout << "Unesite broj koji želite ukloniti iz seta: ";
    cin >> brojZaObrisat;
    brojevi.erase(brojZaObrisat);
    for (auto broj : brojevi) {
        cout << broj << " ";
    }
    cout << endl;

    if (unesi(brojevi, 10)) {
        cout << "Broj 10 unesen u set!" << endl;
    } else {
        cout << "Broj 10 nije unesen u set!" << endl;
    }
}