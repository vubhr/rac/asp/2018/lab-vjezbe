#include <iostream>
#include <map>

using std::cout;
using std::cin;
using std::endl;
using std::pair;
using std::map;

bool unesi(map<int, int>& rezultati, int student, int rezultat) {
    auto rezultatUnosa = rezultati.insert(pair<int,int>(student,rezultat));
    return rezultatUnosa.second;
}

int brojBodova(map<int, int>& rezultati, int student) {
    auto pronadji = rezultati.find(student);
    if (pronadji == rezultati.end()) {
        return -1;
    } else {
        return rezultati[student];
    }
}

void prolaz(map<int, int>& rezultati, int prag) {
    for (auto it = rezultati.begin(); it != rezultati.end(); ++it) {
        if (it->second >= prag) {
            cout << it->first << endl;
        }
    }
}

int main() {
    map<int, int> rezultati;
    unesi(rezultati, 456, 20);
    unesi(rezultati, 501, 15);
    unesi(rezultati, 503, 25);

    cout << brojBodova(rezultati, 456) << endl;
    cout << brojBodova(rezultati, 500) << endl;

    prolaz(rezultati, 20);
}