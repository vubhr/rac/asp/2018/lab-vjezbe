#include <iostream>
#include <vector>

using std::vector;
using std::cout;
using std::endl;

int zbroji(vector<int>& nums) {
	if (nums.size() == 2) {
		return nums[0] + nums[1];
	}
	else if (nums.size() == 1) {
		return nums[0];
	}

	int mid = nums.size() / 2;
	vector<int> left;
	vector<int> right;

	for (int i = 0; i < mid; i++) {
		left.push_back(nums[i]);
	}
	for (int i = 0; i < (nums.size()) - mid; i++) {
		right.push_back(nums[mid + i]);
	}
	return zbroji(left) + zbroji(right);
}

int main() {
	vector<int> brojevi = { 2, 5, 4, 3, 8 };
	cout << zbroji(brojevi) << endl;
}
