#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

void prikazi(const vector<int>& v) {
	for (auto element : v) {
		if (element == 0) {
			cout << "_";
		} else {
			cout << "#";
		}
		cout << " ";
	}
	cout << endl;
}

bool backtrackRijesi(vector<vector<int> >& lab, vector<vector<int> >& rjesenje, int x, int y) {
	if (x == lab.size() - 1 && y == lab.size() - 1)
	{
		rjesenje[x][y] = 1;
		return true;
	}

	if (x >= 0 && x < lab.size() && y >= 0 && y < lab.size() && lab[x][y] == 1) {
		rjesenje[x][y] = 1;

		if (backtrackRijesi(lab, rjesenje, x + 1, y) == true) {
			return true;
		}
		if (backtrackRijesi(lab, rjesenje, x, y + 1) == true) {
			return true;
		}
		rjesenje[x][y] = 0;
		return false;
	}
	return false;
}

void nadjiIzlaz(vector<vector<int> >& lab) {
	vector<vector<int> > rjesenje(lab.size(), vector<int>(lab.size()));

	if (backtrackRijesi(lab, rjesenje, 0, 0)) {
		cout << "Rjesenje je:" << endl;
		for (auto redak : rjesenje) {
			prikazi(redak);
		}
		return;
	}
	cout << "Nema rjesenja." << endl;
}

int main() {
	vector<vector<int> > labirint = {
		{ 1, 1, 1, 1, 0},
		{ 0, 0, 0, 1, 0},
		{ 0, 1, 1, 1, 0},
		{ 0, 1, 0, 0, 1},
		{ 0, 1, 1, 1, 1}
	};
    
	cout << "Labirint:" << endl;
	for (auto redak : labirint) {
		prikazi(redak);
	}
	cout << endl;

	nadjiIzlaz(labirint);
}