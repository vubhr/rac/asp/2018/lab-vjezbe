#include <iostream>

using std::cout;
using std::endl;

struct cvor {
    int vrijednost;
    cvor* lijevo;
    cvor* desno;
};

cvor* stvoriCvor(int vrijednost) {
    cvor* c = new cvor;
    c->vrijednost = vrijednost;
    c->lijevo = NULL;
    c->desno = NULL;
    return c;
}

int main() {
    cvor* korijen = stvoriCvor(10);

    cout << korijen->vrijednost << endl;
}