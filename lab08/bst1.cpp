#include <iostream>

using std::cout;
using std::endl;

struct cvor {
    int vrijednost;
    cvor* lijevo;
    cvor* desno;
};

int main() {
    cvor* korijen = new cvor;
    korijen->vrijednost = 10;
    korijen->lijevo = NULL;
    korijen->desno = NULL;

    cout << korijen->vrijednost << endl;
}