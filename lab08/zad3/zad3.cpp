#include <iostream>
#include "Stablo.h"

using std::cout;
using std::vector;

int main() {
    Stablo stablo;
    stablo.unesi(2);
    stablo.unesi(5);
    stablo.unesi(7);
    stablo.unesi(6);
    stablo.unesi(3);
    stablo.unesi(9);

    vector<int> sortiranoStablo;
    stablo.inorder(sortiranoStablo);
    
    for (auto vrijednost : sortiranoStablo) {
        cout << vrijednost << " ";
    }
}