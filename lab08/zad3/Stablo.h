#include <vector>

struct cvor {
    int vrijednost;
    cvor* lijevo;
    cvor* desno;
};

class Stablo {
public:
    Stablo() {
        korijen = nullptr;
    }
    void unesi(int);
    void inorder(std::vector<int>&);
private:
    cvor* stvoriCvor(int);
    void unesi(cvor*, int);
    void inorder(cvor*, std::vector<int>&);
    cvor* korijen;
};