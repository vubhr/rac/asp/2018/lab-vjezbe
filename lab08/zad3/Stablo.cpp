#include "Stablo.h"

using std::vector;

cvor* Stablo::stvoriCvor(int vrijednost) {
    cvor* c = new cvor;
    c->vrijednost = vrijednost;
    c->lijevo = nullptr;
    c->desno = nullptr;
    return c;
}

void Stablo::unesi(int vrijednost) {
    if (korijen == nullptr) {
        korijen = stvoriCvor(vrijednost);
    } else {
        unesi(korijen, vrijednost);
    }
}

void Stablo::unesi(cvor* korijen, int vrijednost) {
    if (vrijednost < korijen->vrijednost) {
        if (korijen->lijevo == nullptr) {
            cvor* c = stvoriCvor(vrijednost);
            korijen->lijevo = c;
        } else {
            unesi(korijen->lijevo, vrijednost);
        }
    } else {
        if (korijen->desno == nullptr) {
            cvor* c = stvoriCvor(vrijednost);
            korijen->desno = c;
        } else {
            unesi(korijen->desno, vrijednost);
        }
    }
}

void Stablo::inorder(vector<int>& v) {
    inorder(korijen, v);
}

void Stablo::inorder(cvor* korijen, vector<int>& v) {
    if (korijen == nullptr) {
        return;
    }
    inorder(korijen->lijevo, v);
    v.push_back(korijen->vrijednost);
    inorder(korijen->desno, v);
}
