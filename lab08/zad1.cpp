#include <iostream>

using std::cout;
using std::endl;

struct cvor {
    int vrijednost;
    cvor* lijevo;
    cvor* desno;
};

cvor* stvoriCvor(int vrijednost) {
    cvor* c = new cvor;
    c->vrijednost = vrijednost;
    c->lijevo = NULL;
    c->desno = NULL;
    return c;
}

void unesi(cvor* korijen, int vrijednost) {
    if (vrijednost < korijen->vrijednost) {
        if (korijen->lijevo == NULL) {
            cvor* c = stvoriCvor(vrijednost);
            korijen->lijevo = c;
        } else {
            unesi(korijen->lijevo, vrijednost);
        }
    } else {
        if (korijen->desno == NULL) {
            cvor* c = stvoriCvor(vrijednost);
            korijen->desno = c;
        } else {
            unesi(korijen->desno, vrijednost);
        }
    }
}

void preorder(cvor* c) {
    if (c == NULL) {
        return;
    }
    cout << c->vrijednost << endl;
    preorder(c->lijevo);
    preorder(c->desno);
}

void inorder(cvor* c) {
    if (c == NULL) {
        return;
    }
    inorder(c->lijevo);
    cout << c->vrijednost << endl;
    inorder(c->desno);
}

void postorder(cvor* c) {
    if (c == NULL) {
        return;
    }
    postorder(c->lijevo);
    postorder(c->desno);
    cout << c->vrijednost << endl;
}

int main() {
    cvor* korijen = stvoriCvor(10);

    unesi(korijen, 5);
    unesi(korijen, 2);
    unesi(korijen, 12);
    unesi(korijen, 11);
    unesi(korijen, 7);

    inorder(korijen);
}