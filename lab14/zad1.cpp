#include <iostream>
#include <fstream>
#include <string>

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::string;
using std::to_string;

void rleCompress(string file) {
    ifstream datotekaUlaz(file);
    string ulaz;
    string izlaz;

    string linija;
    if (datotekaUlaz.is_open()) {
        while (getline(datotekaUlaz, linija)) {
            ulaz += linija.substr(0, linija.length() - 1);
        }
        datotekaUlaz.close();
    }

    for (int i = 0; i < ulaz.length(); i++) {
        int brojac = 1;
        while (i < ulaz.length() - 1 && ulaz[i] == ulaz[i + 1]) {
            brojac++;
            i++;
        }

        izlaz += ulaz[i] + to_string(brojac);
    }
    cout << izlaz;

    ofstream datotekaIzlaz(file + ".rle");
    if (datotekaIzlaz.is_open()) {
        datotekaIzlaz << izlaz;
        datotekaIzlaz.close();
    }

}

int main() {
    rleCompress("ulaz.txt");

}