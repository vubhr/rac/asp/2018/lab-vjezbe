#include <iostream>
#include <fstream>
#include <string>
#include <map>

using std::cout;
using std::endl;
using std::map;
using std::ifstream;
using std::string;
using std::stoi;
using std::to_string;

map<char, int> huffmanFreq(string file) {
    ifstream datotekaUlaz(file);
    string ulaz;
    map<char, int> frekvencija;

    string linija;
    if (datotekaUlaz.is_open()) {
        while (getline(datotekaUlaz, linija)) {
            ulaz += linija.substr(0, linija.length() - 1);
        }
        datotekaUlaz.close();
    }

    for (int i = 0; i < ulaz.length(); i++) {
        int brojac = 1;
        while (i < ulaz.length() - 1 && ulaz[i] == ulaz[i + 1]) {
            brojac++;
            i++;
        }
        auto postojiZnak = frekvencija.find(ulaz[i]);
        if (postojiZnak != frekvencija.end()) {
            frekvencija[ulaz[i]] = frekvencija[ulaz[i]] + brojac;
        } else {
            frekvencija[ulaz[i]] = brojac;
        }
    }
    return frekvencija;
}

int main() {
    map<char, int> freq = huffmanFreq("ulaz.txt");
    for (auto element : freq) {
        cout << element.first << ":"  << element.second << endl;
    }
}