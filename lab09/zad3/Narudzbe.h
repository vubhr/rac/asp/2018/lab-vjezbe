#include <string>
#include <queue>

using std::string;
using std::priority_queue;

class Narudzba {
public:
    Narudzba(string, int, bool);
    bool operator<(Narudzba n) const;
    string nazivJela();
    string naziv;
private:
    int kolicina;
    int prioritet;
};
