#include <iostream>
#include <queue>
#include "Narudzbe.h"

using std::cout;
using std::endl;
using std::string;
using std::priority_queue;

int main() {
    priority_queue<Narudzba> narudzbe;

    narudzbe.push(Narudzba("Pljeskavice", 1, false));
    narudzbe.push(Narudzba("Kobasice", 1, false));
    narudzbe.push(Narudzba("Flekice s kupusom", 4, true));
    narudzbe.push(Narudzba("Cevapi", 2, false));
    narudzbe.push(Narudzba("Janjetina", 1, true));
    narudzbe.push(Narudzba("Janjetina2", 1, true));
    narudzbe.push(Narudzba("Janjetina3", 1, true));

    while (!narudzbe.empty()) {
        cout << narudzbe.top().naziv << endl;
        narudzbe.pop();
    }
}