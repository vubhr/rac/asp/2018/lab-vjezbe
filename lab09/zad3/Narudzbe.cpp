#include <iostream>
#include "Narudzbe.h"

using std::cout;
using std::string;
using std::endl;
using std::priority_queue;

Narudzba::Narudzba(string naziv, int kolicina, bool VIP) {
    this->naziv = naziv;
    this->kolicina = kolicina;
    if (VIP) {
        prioritet = 1;
    } else {
        prioritet = 0;
    }
}

string Narudzba::nazivJela() {
    return naziv;
}

bool Narudzba::operator<(Narudzba n) const {
    if (n.prioritet >= prioritet) {
        return true;
    }
    return false;
}