#include <iostream>
#include <queue>

using std::cout;
using std::endl;
using std::priority_queue;

int main() {
    priority_queue<int> gomila;
    gomila.push(7);
    gomila.push(3);
    gomila.push(12);
    gomila.push(8);
    gomila.push(9);
    gomila.push(4);
    gomila.push(6);
    gomila.push(2);

    gomila.push(10);

    gomila.pop();
    gomila.pop();

    cout << gomila.top() << endl;
    gomila.pop();
    cout << gomila.top() << endl;
}